package clases;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.Vector;

/**
 * 
 * @autor kike
 * 
 */
public class FicherosSecuenciales {
	/**
	 * Declaracion e inicializacion de la clase Scanner
	 */
	static Scanner input = new Scanner(System.in);
	/**
	 * Atributo archivo
	 */
	private String archivo;

	/**
	 * Constructor
	 * 
	 * @param nombre del archivo
	 */
	public FicherosSecuenciales(String archivo) {
		this.archivo = archivo;
	}

	/**
	 * Metodo para dar de alta un vinilo
	 */
	public void altaVinilo() {
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			PrintWriter f = new PrintWriter(new FileWriter(archivo, true));
			System.out.println("Introduce el nombre del vinilo (* para acabar)");
			String linea = in.readLine();
			while (!linea.equalsIgnoreCase("*")) {
				f.println(linea);
				linea = in.readLine();
			}
			System.out.println("Registro completado");
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Metodo para visualizar el fichero
	 */
	public void visualizarArchivo() {
		try {
			String linea;
			System.out.println("ARCHIVO: " + this.archivo);
			BufferedReader fuente;
			fuente = new BufferedReader(new FileReader(this.archivo));
			linea = fuente.readLine();
			while (linea != null) {
				System.out.println(linea);
				linea = fuente.readLine();
			}
			fuente.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Metodo para buscar una cadena en el fichero pedido por teclado
	 */
	public void buscarDato() {
		String valorABuscar;
		String linea;
		System.out.println("Introduce el nombre del vinilo a buscar");
		valorABuscar = input.nextLine();
		Vector<String> v = new Vector<String>();
		try {
			BufferedReader fuente = new BufferedReader(new FileReader(this.archivo));
			linea = fuente.readLine();
			while (linea != null) {
				String letra;
				String palabra = "";
				for (int j = 0; j < linea.length(); j++) {
					letra = linea.substring(j, j + 1);
					if (letra.equalsIgnoreCase(" ") == false) {
						palabra = palabra + letra;
					}
				}
				v.add(palabra);
				palabra = "";
				linea = fuente.readLine();
			}
			int contadorPalabra = 0;
			for (int j = 0; j < v.size(); j++) {
				String valor = v.elementAt(j).toString();
				if (valor.equalsIgnoreCase(valorABuscar) == true) {
					contadorPalabra++;

				}
			}
			if (contadorPalabra == 0) {
				System.out.println("El vinilo no se encuentra en el archivo");
			} else {
				System.out.println("valor encontrado -> " + valorABuscar);
				System.out.println("N� de veces: " + contadorPalabra);
			}
			fuente.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fichero no encontrado");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}

	}

	/**
	 * Metodo para mostrar el n�mero de caracteres de una cadena pedida por teclado en el fichero
	 */
	public void mostrarCaracteres() {
		String valorABuscar;
		String linea;
		System.out.println("Introduce el nombre del vinilo a buscar");
		valorABuscar = input.nextLine();
		System.out.println("�Cu�ntos car�cteres quieres mostrar?");
		int cantidad = input.nextInt();
		input.nextLine();
		Vector<String> v = new Vector<String>();
		try {
			BufferedReader fuente = new BufferedReader(new FileReader(this.archivo));
			linea = fuente.readLine();
			while (linea != null) {
				String letra;
				String palabra = "";
				for (int j = 0; j < linea.length(); j++) {
					letra = linea.substring(j, j + 1);
					if (letra.equalsIgnoreCase(" ") == false) {
						palabra = palabra + letra;
					}
				}
				v.add(palabra);
				palabra = "";
				linea = fuente.readLine();
			}
			int contadorPalabra = 0;
			for (int j = 0; j < v.size(); j++) {
				String valor = v.elementAt(j).toString();
				if (valor.equalsIgnoreCase(valorABuscar) == true) {
					contadorPalabra++;

				}
			}
			if (contadorPalabra == 0) {
				System.out.println("El nombre del vinilo no se encuentra en el archivo");
			} else {
				System.out.println(valorABuscar.substring(0, cantidad));
			}
			fuente.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fichero no encontrado");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}

	}

	/**
	 * Metodo que muestra una cadena del fichero pedida por teclado con espacios
	 */
	public void mostrarDatoConEspacios() {
		String valorABuscar;
		String linea;
		System.out.println("Introduce el nombre del vinilo a buscar");
		valorABuscar = input.nextLine();
		Vector<String> v = new Vector<String>();
		try {
			BufferedReader fuente = new BufferedReader(new FileReader(this.archivo));
			linea = fuente.readLine();
			while (linea != null) {
				String letra;
				String palabra = "";
				for (int j = 0; j < linea.length(); j++) {
					letra = linea.substring(j, j + 1);
					if (letra.equalsIgnoreCase(" ") == false) {
						palabra = palabra + letra;
					}
				}
				v.add(palabra);
				palabra = "";
				linea = fuente.readLine();
			}
			int contadorPalabra = 0;
			for (int j = 0; j < v.size(); j++) {
				String valor = v.elementAt(j).toString();
				if (valor.equalsIgnoreCase(valorABuscar) == true) {
					contadorPalabra++;

				}
			}
			if (contadorPalabra == 0) {
				System.out.println("El nombre del vinilo no se encuentra en el archivo");
			} else {
				for (int i = 0; i < valorABuscar.length(); i++) {
					System.out.print(valorABuscar.charAt(i) + " ");
				}
			}
			fuente.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fichero no encontrado");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}

	}

	/**
	 * Metodo que muestra el numero de veces que aparece un caracter de una cadena pedida por teclado, en el fichero
	 */
	public void mostrarNumerocaracteres() {
		String valorABuscar;
		String linea;
		System.out.println("Introduce el nombre del vinilo a buscar");
		valorABuscar = input.nextLine();
		System.out.println("Introduce un caracter del nombre de ese vinilo");
		char caracter = input.nextLine().charAt(0);
		Vector<String> v = new Vector<String>();
		try {
			BufferedReader fuente = new BufferedReader(new FileReader(this.archivo));
			linea = fuente.readLine();
			while (linea != null) {
				String letra;
				String palabra = "";
				for (int j = 0; j < linea.length(); j++) {
					letra = linea.substring(j, j + 1);
					if (letra.equalsIgnoreCase(" ") == false) {
						palabra = palabra + letra;
					}
				}
				v.add(palabra);
				palabra = "";
				linea = fuente.readLine();
			}
			int contadorPalabra = 0;
			for (int j = 0; j < v.size(); j++) {
				String valor = v.elementAt(j).toString();
				if (valor.equalsIgnoreCase(valorABuscar) == true) {
					contadorPalabra++;

				}
			}
			if (contadorPalabra == 0) {
				System.out.println("El nombre del vinilo no se encuentra en el archivo");
			} else {
				int contadorCaracter = 0;
				for (int i = 0; i < valorABuscar.length(); i++) {
					if (valorABuscar.charAt(i) == caracter) {
						contadorCaracter++;
					}
				}
				System.out.println("La cantidad de veces que aparece el caracter " + caracter + " en la cadena es "
						+ contadorCaracter);
			}
			fuente.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fichero no encontrado");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}

	}

	/**
	 * Metodo que muestra el porcentaje que hay de cada una de las vocales, respecto al total de caracteres de la cadena pedida por teclado, en el fichero
	 */
	public void mostrarPorcentajeVocales() {
		String valorABuscar;
		String linea;
		int contadorA = 0;
		int contadorE = 0;
		int contadorI = 0;
		int contadorO = 0;
		int contadorU = 0;
		System.out.println("Introduce el nombre del vinilo a buscar");
		valorABuscar = input.nextLine();
		Vector<String> v = new Vector<String>();
		try {
			BufferedReader fuente = new BufferedReader(new FileReader(this.archivo));
			linea = fuente.readLine();
			while (linea != null) {
				String letra;
				String palabra = "";
				for (int j = 0; j < linea.length(); j++) {
					letra = linea.substring(j, j + 1);
					if (letra.equalsIgnoreCase(" ") == false) {
						palabra = palabra + letra;
					}
				}
				v.add(palabra);
				palabra = "";
				linea = fuente.readLine();
			}
			int contadorPalabra = 0;
			for (int j = 0; j < v.size(); j++) {
				String valor = v.elementAt(j).toString();
				if (valor.equalsIgnoreCase(valorABuscar) == true) {
					contadorPalabra++;

				}
			}
			if (contadorPalabra == 0) {
				System.out.println("El nombre del vinilo no se encuentra en el archivo");
			} else {
				for (int i = 0; i < valorABuscar.length(); i++) {
					char caracter = valorABuscar.charAt(i);

					if (caracter == 'a') {
						contadorA++;
					} else if (caracter == 'e') {
						contadorE++;
					} else if (caracter == 'i') {
						contadorI++;
					} else if (caracter == 'o') {
						contadorO++;
					} else if (caracter == 'u') {
						contadorU++;
					}
				}
				System.out.println("A: " + ((double) contadorA / valorABuscar.length() * 100) + " %");
				System.out.println("E: " + ((double) contadorE / valorABuscar.length() * 100) + " %");
				System.out.println("I: " + ((double) contadorI / valorABuscar.length() * 100) + " %");
				System.out.println("O: " + ((double) contadorO / valorABuscar.length() * 100) + " %");
				System.out.println("U: " + ((double) contadorU / valorABuscar.length() * 100) + " %");

			}

			fuente.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fichero no encontrado");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}

	}

}
