package clases;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.util.Scanner;
import java.util.Vector;

/**
 * 
 * @autor kike
 * 
 */
public class FicherosAccesoAleatorio {
	/**
	 * Atributo archivo
	 */
	String archivo;

	/**
	 * Constructor
	 * 
	 * @param nombre del archivo
	 */
	public FicherosAccesoAleatorio(String nombre) {
		this.archivo = nombre;
	}

	/**
	 * Metodo para dar de alta un artista
	 */
	public void altaArtista() {
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			f.seek(f.length());
			String respuesta = "";
			do {
				System.out.println("Nombre: ");
				String nombre = in.readLine();
				nombre = formatoNombre(nombre, 20);
				f.writeUTF(nombre);
				System.out.println("�Deseas continuar (si/no)?");
				respuesta = in.readLine();
			} while (respuesta.equalsIgnoreCase("si"));
			f.close();
		} catch (IOException e) {
			System.out.println("error");
		}
	}

	/**
	 * Metodo para convertir la cadena al tama�o indicado
	 * 
	 * @param nombre del artista y tama�o de la cadena
	 * @return el nombre del artista
	 */
	private String formatoNombre(String nombre, int lon) {
		if (nombre.length() > lon) {
			return nombre.substring(0, lon);
		} else {
			for (int i = nombre.length(); i < lon; i++) {
				nombre = nombre + " ";
			}
		}
		return nombre;
	}

	/**
	 * Metodo para visualizar el fichero
	 */
	public void visualizarArchivo() {
		try {
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			String nombre = "";
			boolean finFichero = false;
			do {
				try {
					nombre = f.readUTF();
					System.out.println(nombre);
				} catch (EOFException e) {
					finFichero = true;
					f.close();
				}

			} while (!finFichero);
		} catch (IOException e) {
			System.out.println("error");
		}
	}

	/**
	 * Metodo para modificar una cadena del fichero
	 */
	public void modificarArchivo() {
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			System.out.println("�Qu� artista quieres modificar?");
			String dato = in.readLine();
			System.out.println("Introduce el nuevo nombre del artista:");
			String nuevoDato = in.readLine();
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			String nombre;
			boolean finFichero = false;
			boolean valorEncontrado = false;
			do {
				try {
					nombre = f.readUTF();
					if (nombre.trim().equalsIgnoreCase(dato)) {
						f.seek(f.getFilePointer() - 22);
						nuevoDato = formatoNombre(nuevoDato, 20);
						f.writeUTF(nuevoDato);
						valorEncontrado = true;
					}
				} catch (EOFException e) {
					f.close();
					finFichero = true;
				}
			} while (!finFichero);
			if (valorEncontrado == false) {
				System.out.println("El artista no est� en el fichero");
			} else {
				System.out.println("El artista ha sido modificado");
			}
		} catch (IOException e) {
			System.out.println("Error");
		}
	}

	/**
	 * Metodo que cuenta las lineas del fichero
	 */
	public void contadorArtistas() {

		try {
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			String nombre = "";
			boolean finFichero = false;
			int contador = 0;
			do {
				try {
					nombre = f.readUTF();
					contador++;
				} catch (EOFException e) {
					finFichero = true;
					System.out.println("N� de artistas: " + contador);
					f.close();
				}

			} while (!finFichero);
		} catch (IOException e) {
			System.out.println("error");
		}
	}

	/**
	 * Metodo que cambia las vocales 'E' por 'I' del fichero
	 */
	public void cambiarVocales() {
		System.out.println("M�todo que cambia las vocales 'E' por las 'I'");
		try {
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			String nombre = "";
			boolean finFichero = false;
			do {
				try {
					nombre = f.readUTF();
					f.seek(f.getFilePointer() - 22);
					nombre = formatoNombre(nombre, 20);
					nombre = nombre.replaceAll("e", "i");
					f.writeUTF(nombre);
					System.out.println(nombre);
				} catch (EOFException e) {
					finFichero = true;
					f.close();
				}

			} while (!finFichero);
		} catch (IOException e) {
			System.out.println("error");
		}
	}

	/**
	 * Metodo que introduce un salto de linea en la ultima posicion
	 */
	public void saltoDeLinea() {
		try {
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			f.seek(f.length());
			f.writeUTF("\n");
			f.close();
			System.out.println("Fichero actualizado");
		} catch (IOException e) {
			System.out.println("Error de datos");
		}
	}
}
