package programa;

import java.util.Scanner;

import clases.FicherosAccesoAleatorio;
import clases.FicherosSecuenciales;

public class Programa {

	public static String archivoS = "Secuencial.txt";
	public static String archivoA = "Aleatorio.txt";

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		FicherosSecuenciales ficheroS = new FicherosSecuenciales(archivoS);
		FicherosAccesoAleatorio ficheroA = new FicherosAccesoAleatorio(archivoA);
		int opcion = 0;
		System.out.println(" \n Practica 7 __________________________  Enrique Mu�oz");
		menuPrincipal();
		do {

			opcion = input.nextInt();

			switch (opcion) {

			case 1:

				int menu1 = 0;

				do {
					System.out.println("\n ____________________________________________________");
					System.out.println(" \n 		MEN� FICHEROS SECUENCIALES");
					System.out.println("\n 1.- Alta vinilo");
					System.out.println(" 2.- Visualizar fichero");
					System.out.println(" 3.- Buscar en fichero");
					System.out.println(" 4.- Acci�n extra n� 1: Mostrar caracteres dato");
					System.out.println(" 5.- Acci�n extra n� 2: Mostrar dato con espacios");
					System.out.println(" 6.- Acci�n extra n� 3: Mostrar n� veces caracter dato");
					System.out.println(" 7.- Acci�n extra n� 4: Mostrar % de vocales");
					System.out.println(" 8.- Volver atr�s");
					System.out.print("\n Ingrese Opci�n: ");
					System.out.println("\n ____________________________________________________");
					menu1 = input.nextInt();

					switch (menu1) {

					case 1:
						ficheroS.altaVinilo();
						break;
					case 2:
						ficheroS.visualizarArchivo();
						break;
					case 3:
						ficheroS.buscarDato();
						break;
					case 4:
						ficheroS.mostrarCaracteres();
						break;
					case 5:
						ficheroS.mostrarDatoConEspacios();
						break;
					case 6:
						ficheroS.mostrarNumerocaracteres();
						break;
					case 7:
						ficheroS.mostrarPorcentajeVocales();
						break;
					case 8:
						menuPrincipal();
					default:

					}

				} while (menu1 != 8);

				break;

			case 2:

				int menu2 = 0;

				do {
					System.out.println("\n ____________________________________________________");
					System.out.println("   	  MEN� FICHEROS ACCESO ALEATORIO");
					System.out.println("\n 1. Alta artista");
					System.out.println(" 2. Visualizar fichero");
					System.out.println(" 3. Modificar artista");
					System.out.println(" 4. Acci�n extra n� 1: Cambiar vocales");
					System.out.println(" 5. Acci�n extra n� 2: Contador de artistas");
					System.out.println(" 6. Acci�n extra n� 3: Introducir salto de l�nea");
					System.out.println(" 7. Volver atr�s");
					System.out.print("\n Ingrese Opci�n: ");
					System.out.println("\n ____________________________________________________");

					menu2 = input.nextInt();

					switch (menu2) {

					case 1:
						ficheroA.altaArtista();
						break;
					case 2:
						ficheroA.visualizarArchivo();
						break;
					case 3:
						ficheroA.modificarArchivo();
						break;
					case 4:
						ficheroA.cambiarVocales();
						break;
					case 5:
						ficheroA.contadorArtistas();
						break;
					case 6:
						ficheroA.saltoDeLinea();
						break;
					case 7:
						menuPrincipal();
					default:
						

					}

				} while (menu2 != 7);

				break;
			case 3:
				System.out.println("Hasta luego");
				System.exit(0);

				break;
			default:
				System.out.println("Opci�n no v�lida, vuelva a intentarlo.");
				menuPrincipal();
			}
		} while (opcion != 3);

		input.close();
	}

	public static void menuPrincipal() {

		System.out.println("\n ____________________________________________________");
		System.out.println("           	 MEN� PRINCIPAL");
		System.out.println("\n 1.Ficheros Secuenciales");
		System.out.println("\n 2.Ficheros Acceso Aleatorio");
		System.out.println("\n 3.Salir");
		System.out.print("\n Ingrese Opci�n: ");
		System.out.println("\n ____________________________________________________");
	}

}